#!/usr/bin/env python3

import os
import sys
import numpy as np
import wave
import math
from stt import Model

sys.path.insert(1, 'scripts/')
from model_server import ModelServer
from matrix import ConfusionMatrix

# The intial port. Will increment for each
# model server that is opened
initport = 8850

class LanguageModel:
    """ A collection of different models for a single language
    """

    def __init__(self, tawa: str = None, coqui: str= None, wavs: str = None):
        """
        If a coqui model if provided, then it will load that model
        If a model file for Tawa is provided it will start up a model server.
        If a path to a set of test wavs are provided it will store that.
        """
        global initport
        modelpath = "/home/prv/.local/share/coqui/models/"
        if coqui:
            self.sttmodel = Model(f"{modelpath}{coqui}/model.tflite") 
        if tawa:
            self.modelserver = ModelServer(port=initport, modelfile=tawa)
            initport += 1
        if wavs:
            self.wavs = wavs

    def analyse_audio(self, audio):
        text = ""
        bpc = float("inf")
        if hasattr(self, 'sttmodel'):
            text = self.get_stt(audio)
        if hasattr(self, 'modelserver') and text:
            bpc = self.get_bpc(text)
        return (text, bpc)

    def get_stt(self, audio):
        return self.sttmodel.stt(audio)

    def get_bpc(self, text: str) -> float:
        if not hasattr(self, 'modelserver'):
            return 0.0
        return self.modelserver.get_bpc(text)

# Object containing all of the available
# language models
models = {
#    "English": LanguageModel(
#        coqui = "English STT v0.9.3",
#    ),
    "German": LanguageModel(
        coqui = "German STT v0.9.0",
        tawa  = "models/de.dat",
        wavs  = "build/de/wav"
    ),
    "Italian": LanguageModel(
        coqui = "Italian STT 2020.8.7",
        tawa  = "models/it.dat",
        wavs  = "build/it/wav"
    ),
    "Spanish": LanguageModel(
        coqui = "Spanish STT v0.0.1",
        tawa  = "models/es.dat",
        wavs  = "build/es/wav"
    ),
    "Welsh": LanguageModel(
        coqui = "Welsh STT v21.03",
        tawa  = "models/cy.dat",
        wavs  = "build/cy/wav"
    )
}

def test_models(results: ConfusionMatrix = ConfusionMatrix(), length_results: dict = {}, max=1000) -> ConfusionMatrix:
    for expected, model in models.items():

        # If the language doesn't contain test files
        # we can simply skip it
        if not hasattr(model, 'wavs'):
            continue

        print(f"\nTesting sound files for {expected}")
        processed = 0
        # Iterate over all of the test files
        with os.scandir(model.wavs) as it:
            for file in it:
                if processed >= max:
                    break
                processed += 1

                # Print basic status information
                print(f"\n({processed*100/max:5.1f}%|{results.get_percentage_correct():5.1f}%) Testing {file.name}")

                # Load in the audio data
                fin = wave.open(f"{file.path}", "rb")
                audio = np.frombuffer(fin.readframes(fin.getnframes()), np.int16)
                fin.close()

                smallest_bpc = float('inf')
                smallest_lang = ""
                stringlength = 0
                langs = 0

                # Loop through all of models to see
                # which stt-tawa combo is best at compressing
                # the audio
                for lang, testmodel in models.items():
                    stt, bpc = testmodel.analyse_audio(audio)
                    print("{:8} : {:.3f} : {}".format(lang, bpc, stt))
                    if len(stt) > 0:
                        stringlength += len(stt)
                        langs += 1
                    if not smallest_lang or bpc < smallest_bpc:
                        smallest_lang = lang
                        smallest_bpc = bpc

                # Add the language with the smallest bpc to
                # the confusion matrix as the actual classification
                results.add_data(expected, smallest_lang)

                # Add data based on length
                if langs != 0:
                    avglength = str(math.floor(stringlength / langs))
                    if avglength not in length_results:
                        length_results[avglength] = {
                            "total": 0,
                            "correct": 0
                        }
                    length_results[avglength]['total'] += 1
                    if expected == smallest_lang:
                        length_results[avglength]['correct'] += 1

    # Return the results
    return results


def print_results(results: ConfusionMatrix, length_results: dict):
    """Prints the results from the confusion matrix and the
    length results dictionary.

    Params:
        results: The confusion matrix with the results
        length_results: The dictionary with the results based on lengths    
    """
    print("\nSpeech to text results\n==========================\n")

    # Prints the results based on length
    for key, value in length_results.items():
        print(f"{key}\t{value['total']}\t{value['correct']}")

    # Prints the confusion matrix itself
    print()
    results.print_confusion_matrix()


# The variables to store the results in
results = ConfusionMatrix()
length_results = {}

# Add the print function to atexit
# in case the thing crashes or we interupt it
import atexit
atexit.register(print_results, results, length_results)

# Run the tests
test_models(results=results, length_results=length_results, max=2000)



