#!/usr/bin/env python3

import os
import sys
import math
import numpy

sys.path.insert(1, 'scripts/')
from error_matrix import ErrorMatrix
from matrix import ConfusionMatrix

class LengthResults:
    
    def __init__(self) -> None:
        self.results = {}
    
    def add(self, length: int, correct: bool) -> None:
        if length not in self.results:
            self.results[length] = { "correct": 0, "total": 0 }
        if correct:
            self.results[length]['correct'] += 1
        self.results[length]['total'] += 1


    def export(self, path: str) -> None:
        with open(path, "w") as output:
            for (length, value) in sorted(self.results.items(), key=lambda x: int(x[0])):
                output.write(f"{length}\t{value['correct']}\t{value['total']}\n")

class ModelPairCollection:

    def __init__(self, name: str, models) -> None:
        self.name = name
        self.matrix = ConfusionMatrix()
        self.length_data = LengthResults()
        self.models = { model: ErrorMatrix() for model in models }
    
    def add_data(self, file, expected, results) -> None:
        # If we don't have the expected language we can simply return
        if expected not in [ model.lang for model in self.models.keys() ]:
            return 

        results = [ model for _, model in results if model in self.models]
        self.matrix.add_data(expected, results[0].lang)

        avg_length = str(math.floor(numpy.mean([ model.length[file] for model in self.models if model.length[file] != float('Inf') ])))
        self.length_data.add(avg_length, expected == results[0].lang)

        found_correct = False
        correct = True

        # Loop through and add data to all of the models
        for model in results:
            if model.lang != expected:
                correct = False
                if not found_correct:
                    self.models[model].fp += 1
                else:
                    self.models[model].tn += 1
            else:
                found_correct = True
                if correct:
                    self.models[model].tp += 1
                else:
                    self.models[model].fn += 1

    def export(self) -> None:
        with open(f"test/results/{self.name}_model_results.csv", "w") as output:
            fields = {
                "lang": lambda x: x.id,
                "avg_cor": lambda x: x.average_correct(),
                "avg_inc": lambda x: x.average_wrong(),
                "avg_dist": lambda x: x.average_wrong() - x.average_correct(),
                "std_cor": lambda x: x.std_correct(),
                "std_inc": lambda x: x.std_wrong(),
                "acc": lambda x: self.models[x].accuracy(),
                "rec": lambda x: self.models[x].recall(),
                "pre": lambda x: self.models[x].precision(),
                "f1": lambda x: self.models[x].f1_score(),
                "mcc": lambda x: self.models[x].mcc(),
                "ba": lambda x: self.models[x].balanced_accuracy(),
                "low": lambda x: x.low(),
                "high": lambda x: x.high()
            }

            output.write("\t".join(fields.keys()))
            output.write("\n")
            for model in self.models:
                output.write("\t".join([ str(value(model)) for _,value in fields.items()]))
                output.write("\n")
        
        self.length_data.export(f"test/results/{self.name}_length_results.csv")


class ModelPair:
    def __init__(self, filename: str) -> None:
        self.path = filename
        self.id = filename.split(".")[0]

        idparts = self.id.split("-")
        self.lang = idparts[0]
        self.subid = "-".join(idparts[1:]) if len(idparts) > 1 else None
        self.data = {}
        self.length = {}
        with open(f"test/bpc/{self.path}") as input:
            while True:
                line = input.readline()
                if not line:
                    break
                data = line.split("\t") 
                self.data[data[0]] = float(data[1])
                self.length[data[0]] = int(data[2])

    def std_correct(self):
        return numpy.std([float(val) for file, val in self.data.items() if val != float("inf") and f"voice_{self.lang}" in file ])

    def std_wrong(self):
        return numpy.std([float(val) for file, val in self.data.items() if val != float("inf") and f"voice_{self.lang}" not in file ])

    def average_correct(self) -> float:
        return numpy.mean([float(val) for file, val in self.data.items() if val != float("inf") and f"voice_{self.lang}" in file ])
    
    def average_wrong(self) -> float:
        return numpy.mean([float(val) for file, val in self.data.items() if val != float("inf") and f"voice_{self.lang}" not in file ])

    def low(self):
        return self.average_correct()-(self.std_correct()/2)

    def high(self):
        return self.average_wrong()+(self.std_wrong()/2)


def main():
    models = []
    for file in os.listdir("test/bpc/"):
        models.append(ModelPair(file))

    models.sort(key=lambda x: x.path)

    commonfiles = set(models[0].data.keys())
    for model in models[1:]:
        commonfiles = commonfiles & set(model.data.keys())

    print(f"Found {len(commonfiles)} common files")

    # Find all of the langauges that has both a model and test files
    languages = set([ model.lang for model in models ])
    print(sorted(languages))
    commonlangs = set([ file.split("_")[2][:2] for file in commonfiles ])
    print(sorted(commonlangs))
    languages = languages & commonlangs
    print(sorted(languages))

    # Remove all models without common testfiles
    models = [ model for model in models if model.lang in languages ]

    collections =[
        ModelPairCollection("full", models),
        ModelPairCollection("f1", [ model for model in models if model.id in [
            "br-op",
            "cy-ceg",
            "de-aa-b",
            "es-full",
            "eu",
            "fr-cv-full",
            "fy",
            "ga",
            "nl",
            "it-ja-b",
            "pt"
        ]]),
        ModelPairCollection("filtered", [ model for model in models if model.id in [
            "br-op",
            "cy-full",
            "de-ja-l",
            "es-full",
            "eu",
            "fr-ja-full",
            "fy",
            "it-ja-full",
            "pt"
        ]]),
        ModelPairCollection("best6", [ model for model in models if model.id in [
            "de-ja-l",
            "en-l",
            "es-full",
            "ca",
            "fr-ja-full",
            "it-ja-full"
        ]]),
        ModelPairCollection("best4", [ model for model in models if model.id in [
            "de-ja-l",
            "en-l",
            "es-full",
            "fr-ja-full"
        ]])
    ]

    for file in commonfiles:
        expected = file.split("_")[2][:2]
        if expected not in languages:
            continue

        results = [ (model.data[file], model) for model in models ]
        results.sort(key=lambda x:x[0])
        for collection in collections:
            collection.add_data(file, expected, results)

    
    for collection in collections:
        print()
        collection.matrix.print_confusion_matrix()
        collection.matrix.print_accuracy()
        collection.export()
    

if __name__ == "__main__":
    main()





