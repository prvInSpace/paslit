#!/usr/bin/env python3

import os
import sys
from threading import Thread

sys.path.insert(1, 'scripts/')
from model_server import ModelServer

# Need a way to access the data (open file)
class STTModel:
    def __init__(self, filename) -> None:
        self.path = f"test/stt/{filename}"
        name = filename.split(".")[0]
        idparts = name.split("-")
        self.lang = idparts[0]
        self.id = idparts[1] if len(idparts) > 1 else None

# TODO: Enable server
class TawaModel:

    def __init__(self, id: str, port: int) -> None:
        name = id.split(".")[0]
        idparts = name.split("-")
        self.lang = idparts[0]
        self.corpus_id = idparts[1] if len(idparts) > 1 else None
        self.server = ModelServer(port=port, modelfile=f"models/dats/{id}")


# TODO: Add output file
# TODO: Read already processed files from output
# Just try running it and see if it is worth it?
class ModelPair:
    def __init__(self, stt: STTModel, tawa: TawaModel) -> None:
        self.stt = stt
        self.tawa = tawa
        self.name = f"{tawa.lang}"
        if stt.id is not None:
            self.name += "-" + stt.id
        if tawa.corpus_id is not None:
            self.name += "-" + tawa.corpus_id

        self.output = open(f"test/bpc/{self.name}.csv", "w")

    def __del__(self):
        """Ensures that the output file
        is closed properly
        """
        if hasattr(self, "output"):
            self.output.close()

    def run(self) -> None:
        with open(self.stt.path, "r") as input:
            while True:
                line = input.readline()
                if not line:
                    break
                data = line.split("\t")
                if len(data) < 2:
                    break
                text = data[1]
                bpc = self.tawa.server.get_bpc(text)
                self.output.write(f"{data[0]}\t{bpc}\t{len(text)}\n")

def main():
    models = {}
    stts = {}

    threads = []

    # Find all of the TAWA models
    print("Loading TAWA models")
    port = 16780
    for file in os.listdir("models/dats/"):
        def func(models, model, port):
            model = TawaModel(file, port)
            if not model.lang in models:
                models[model.lang] = []
            models[model.lang].append(model)
        t = Thread(target=func,args=(models, file, port))
        port += 1
        threads.append(t)
        t.start()
    
    for t in threads:
        t.join()

    # Find all of the STT results
    print("Loading STT Models")
    for file in os.listdir("test/stt/"):
        stt = STTModel(file)
        if not stt.lang in stts:
            stts[stt.lang] = []
        stts[stt.lang].append(stt)

    # Go through for each combination for
    # each language and fetch the bpc
    pairs = []
    for lang, list in models.items():
        for tawa in list:
            for stt in stts[lang]:
                pairs.append(ModelPair(stt, tawa))

    pairs.sort(key=lambda x: x.name)

    for pair in pairs:
        print(f"Compressing: {pair.name}")
        pair.run()

if __name__ == "__main__":
    main()





