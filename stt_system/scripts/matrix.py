
class ConfusionMatrix:

    def __init__(self) -> None:
        self.total = 0
        self.correct = 0
        self.results = {}
        self.possible_values = []


    def get_percentage_correct(self) -> float:
        if self.total == 0:
            return float("inf")
        return self.correct * 100 / self.total

    def add_data(self, expected: str, actual: str):
        """ Adds data to the matrix based on the
        given expected and actual classification
        """
        # Update possible values
        if actual not in self.possible_values:
            self.possible_values.append(actual)
        if expected not in self.possible_values:
            self.possible_values.append(expected)

        # Add cell in matrix if it doesn't exist
        if expected not in self.results:
            self.results[expected] = {}
        if actual not in self.results[expected]:
            self.results[expected][actual] = 0
        
        # Increment the cell
        self.results[expected][actual] += 1

        # Update overarching counter
        self.total += 1
        if expected == actual:
            self.correct += 1


    def print_confusion_matrix(self) -> None:
        """Prints the confusion matrix using the
        data stored in results object. The available
        values are stored in the possible_values variable.
        """
        keys = sorted(self.possible_values)
        print("", end="\t")
        for key in keys:
            print(key, end="\t")
        print()
        for expected in keys:
            if expected in self.results:
                print(expected, end="\t")
                for actual in keys:
                    if actual in self.results[expected]:
                        print(self.results[expected][actual], end="\t")
                    else:
                        print(0, end="\t")
                print()
        print()

    def print_accuracy(self, filtered=[]) -> None:
        """ Prints the overall accuracy of the confusion matrix
        """
        sum = 0
        correct = 0
        incorrect = 0
        for (expected, actual) in [ (key, value) for key, value in self.results.items() if key not in filtered]:
            if expected in actual:
                correct += actual[expected]
            for value in [ value for key, value in actual.items() if key not in filtered ]:
                if value != expected:
                    incorrect += value
                sum += value
        sum = 1 if sum == 0 else sum
        print(f"Recall: {correct*100/sum:.3f}%")

