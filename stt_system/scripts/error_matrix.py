
import math

class ErrorMatrix:
    """ Helper class that stores information
    about a classifiers preformance, and contains
    functions to help extract certain derivations
    from the matrix.
    """

    def __init__(self) -> None:
        """ Initiates the object with empty values"""
        self.tp = 0
        self.fp = 0
        self.fn = 0
        self.tn = 0

    def positive(self) -> int:
        """ Returns the real number of postive cases in
        the data. Calculated as TP + FN
        """
        return self.tp + self.fn

    def negative(self) -> int:
        """ Returns the real number of negative cases in
        the data. Calculated as FP + TN
        """
        return self.fp + self.tn

    def mcc(self) -> float:
        """Calculates the Matthews Correlation Coefficient (MCC)
        for the matrix. This is calculated as

        TP*TN-FP*FN/SQRT((TP+FP)(TP+FN)(TN+FP)(TN+FN))
        """
        return ((self.tp*self.tn) - (self.fp*self.fn)) /\
            math.sqrt((self.tp+self.fp)*(self.tp+self.fn)*(self.tn+self.fp)*(self.tn+self.fn))

    def f1_score(self) -> float:
        """ Calculates the F1 score, or the the harmonic mean
        of precision and sensitivity
        """
        return 2*((self.precision()*self.recall()) / (self.precision()+self.recall()))

    def accuracy(self) -> float:
        return (self.tp + self.tn) / (self.tp + self.tn + self.fn + self.fp)

    def precision(self) -> float:
        return self.tp / (self.tp + self.fp)

    def recall(self) -> float:
        return self.tp / (self.tp + self.fn)

    def selectivity(self) -> float:
        return self.tn / (self.tn + self.fp)

    def negative_predictive_value(self) -> float:
        return self.tn / (self.tn + self.fn)

    def fall_out(self) -> float:
        """ Returns the fall-out or false-positive rate
        of the matrix. Calculated as FP/N or FP/(FP+TN)
        """
        return self.fp / self.negative()

    def balanced_accuracy(self) -> float:
        return (self.recall()+self.selectivity())/2
