#!/usr/bin/env python3

import os
import sys
import numpy as np
import wave
from threading import Thread
from typing import List, Tuple
from model_server import ModelServer
from stt import Model

from model_server import ModelServer

class STTModel:
    def __init__(self, id, coqui) -> None:
        idparts = id.split("-")
        self.name = id
        self.lang = idparts[0]
        self.lang_name = coqui.split()[0]
        self.id = idparts[1] if len(idparts) > 1 else None
        modelpath = "/home/prv/.local/share/coqui/models/"
        self.sttmodel = Model(f"{modelpath}{coqui}/model.tflite")

    def transcribe(self, audio) -> str:
        return self.sttmodel.stt(audio)

class TawaModel:

    def __init__(self, id: str, port: int) -> None:
        name = id.split(".")[0].upper()
        idparts = name.split("-")
        self.lang = idparts[0]
        self.corpus_id = idparts[1] if len(idparts) > 1 else None
        self.server = ModelServer(port=port, modelfile=f"models/dats/{id}")

    def compress(self, text) -> float:
        return self.server.get_bpc(text)

class Result:
    def __init__(self, pair, bpc, text) -> None:
        self.pair = pair
        self.bpc = bpc
        self.text = text

class ModelPair:
    def __init__(self, stt: STTModel, tawa: TawaModel) -> None:
        self.stt = stt
        self.tawa = tawa
        self.lang = tawa.lang
        self.name = tawa.lang
        if stt.id is not None:
            self.name += "-" + stt.id
        if tawa.corpus_id is not None:
            self.name += "-" + tawa.corpus_id

    def run(self,audio) -> Result:
        text = self.stt.transcribe(audio)
        bits = self.tawa.compress(text)
        # print(f"{self.name}: {bits} {text}")
        return Result(self, bits, text)


def load_audio(path):
    fin = wave.open(path, "rb")
    audio = np.frombuffer(fin.readframes(fin.getnframes()), np.int16)
    fin.close()
    return audio
    
def load_models(stt_models_to_load) -> List[ModelPair]:

    tawa_models= {}
    stt_models = {}

    # Array to store active threads in
    threads = []
    port = 16780
    # Initiate all models
    print("Loading TAWA models")
    for file in os.listdir("models/dats/"):
        def func(models, model, port):
            model = TawaModel(file, port)
            if not model.lang in models:
                models[model.lang] = []
            models[model.lang].append(model)
        t = Thread(target=func,args=(tawa_models, file, port))
        port += 1
        threads.append(t)
        t.start()
    
    # Wait for all to start
    print("Waiting for TAWA models to load")
    for thread in threads:
        thread.join()

    print("Loading STT Models")
    for id, model in stt_models_to_load.items():
        def init(models, id, model):
            model = STTModel(id, model)
            if not model.lang in models:
                models[model.lang] = []
            models[model.lang].append(model)
        t = Thread(target=init,args=(stt_models, id, model))
        threads.append(t)
        t.start()

    print("Waiting for STT Models to load")
    for thread in threads:
        thread.join()

    # Create a list of all of the pairs
    pairs = []
    for lang, list in tawa_models.items():
        for tawa in list:
            for stt in stt_models[lang]:
                pairs.append(ModelPair(stt, tawa))
    
    

    print("\nALL MODELS\n==================")
    for pair in pairs:
        print(pair.name)

    return pairs

def main(path):
    audio = load_audio(path)

    models = load_models({
        "BR": "Breton STT v0.1.1",
        "CA": "Catalan STT v0.14.0",
        "CY": "Welsh STT v21.03",
        "DE-AA": "German STT v0.9.0",
        "DE-JA": "German STT v0.0.1",
        "EN": "English STT v1.0.0-large-vocab",
        "ES": "Spanish STT v0.0.1",
        "EU": "Basque STT v0.1.1",
        "FR-CV": "French STT v0.6",
        "FR-JA": "French STT v0.0.1",
        "FY": "Frisian STT v0.1.1",
        "GA": "Irish STT v0.1.1",
        "IT-JA": "Italian STT v0.0.1",
        "IT-MI": "Italian STT 2020.8.7",
        "NL": "Dutch STT v0.0.1",
        "PT": "Portuguese STT v0.1.1"
    })


    models_by_lang = {}
    for model in models:
        if model.lang not in models_by_lang:
            models_by_lang[model.lang] = []
        models_by_lang[model.lang].append(model)


    results = {}
    def thread_func(lang):
        for pair in models_by_lang[lang]:
            results[pair] = pair.run(audio)
            print(f"\rProcessed: {len(results)}/{len(models)}", end="")

    print(f"\nTesting audio file against {len(models)} models")
    threads = []
    for lang in models_by_lang.keys():
        t = Thread(target=thread_func, args=[lang])
        threads.append(t)
        t.start()

    print("Wainting for threads to process")
    for t in threads:
        # print(f"\rProcessed: {len(results)}/{len(models)}", end="")
        t.join()

    
    final_results = sorted(results.values(), key=lambda x: x.bpc)

    print("\n\nProcessing results")
    for result in final_results:
        print("{:10s} | {:5.2f} | {}".format(result.pair.name, result.bpc, result.text))

    print(f"\nMost likely language is: {final_results[0].pair.stt.lang_name}")


path = sys.argv[1]

# Starts the program
if __name__ == "__main__": main(path)
