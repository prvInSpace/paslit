#!/usr/bin/env python3

import os
import sys
import numpy as np
import wave
import math
import time
from stt import Model
from threading import Thread

max_g = int(sys.argv[1]) if len(sys.argv) > 1 else 500

class STTModel:
    """
    """

    def __init__(self, id, coqui):
        """
        """
        self.id = id
        modelpath = "/home/prv/.local/share/coqui/models/"
        resultspath = f"test/stt/{id.lower()}.csv"
        self.sttmodel = Model(f"{modelpath}{coqui}/model.tflite")
        self.previous_files = {}

        # Read previously processed files
        if os.path.exists(resultspath):
            count = 0
            with open(resultspath, "r") as input:
                while True:
                    line = input.readline()
                    if not line:
                        break
                    file = line.split()[0]
                    self.previous_files[file] = None
                    count += 1
            print(f"{id}: Loaded {count} files")
        
        # Reopens the output file in append mode
        self.output = open(resultspath, "a")

    def __del__(self):
        """Ensures that the output file
        is closed properly
        """
        if hasattr(self, "output"):
            self.output.close()

    def has_processed_file(self, filename: str) -> bool:
        """Checks whether the model has processed the
        file before
        """
        return filename in self.previous_files


    def add_sample(self, filename, audio):
        """Takes the audio-data, fetches the
        text from the audio, then appends it to
        the processed data csv file for the
        model.
        """
        if filename in self.previous_files:
            return
        text = self.sttmodel.stt(audio)
        self.output.write(f"{filename}\t{text}\n")
        self.previous_files[filename] = None


 
def test_models(models: dict, maxfiles=1000) -> None:
    """Loop through all of the folders in wavs
    and then loop through all of the files inside each
    individual folders.

    Params:
        models: Dictionary with ID - STTModel pairs
        max: Max number of tests per folder in test/wavs
    """

    langs = sum(1 for _ in os.scandir("../common/wavs"))
    total = langs * maxfiles
    print(f"Total languages: {langs}") 
    print(f"Total files:     {total}")
    start = time.time()
    actually_processed = 0

    # Determine how many files need to be processed
    actually_need_processing = need_processing_count(models, maxfiles)

    with os.scandir("../common/wavs") as langs:
        for lang in langs:
            with os.scandir(lang.path) as it:
                processed = 0
                for file in it:

                    # Calculate time remaining
                    ti = time.time() - start
                    tr = (ti / max(0.00001, actually_processed/max(actually_need_processing,1))) - ti

                    if need_processing(models, file):
                        #print(f"({total_processed:5d}|{(total_processed*100/total):7.1f}%)")
                        print(f"\r({processed:4d}|{(processed*100/maxfiles):5.1f}%)({actually_processed}/{actually_need_processing}|{(actually_processed*100/actually_need_processing):05.2f}%) - Processing {file.name}")
                        print(f"Elapsed: {math.floor(ti/60)} min {ti%60:02.0f} sec | ETA: {math.floor(tr/(60*60))} hr {math.floor(tr/60) % 60} min {tr%60:02.0f} sec", end='')
                        sys.stdout.flush()
                        test_file(models, file)
                        actually_processed += 1
                    processed += 1
                    if processed >= maxfiles:
                        break


def need_processing_count(models, maxfiles):
    """A dry run through the available WAV files to see how many needs processing"""
    files = set()
    with os.scandir("../common/wavs") as langs:
        for lang in langs:
            with os.scandir(lang.path) as it:
                processed = 0
                for file in it:
                    if need_processing(models, file):
                        files.add(file.name)
                    processed += 1
                    if processed >= maxfiles:
                        break
    return len(files)

def need_processing(models: dict, file: os.DirEntry):
    # Try to find a model that needs to process the value
    for model in models.values():
        if not model.has_processed_file(file.name):
            break
    # If no model requires processing we can simply return
    else:
        return False
    return True

def test_file(models: dict, file:os.DirEntry) -> None:

    # Load audio data
    fin = wave.open(f"{file.path}", "rb")
    audio = np.frombuffer(fin.readframes(fin.getnframes()), np.int16)
    fin.close()
    
    # Array to store active threads in
    threads = []
    
    # Start a processing thread for each model that
    # requires processing
    for model in models.values():
        if not model.has_processed_file(file.name):
            t = Thread(target=model.add_sample, args=(file.name, audio))
            threads.append(t)
            t.start()

    # Wait for all of the treads to finish
    for thread in threads:
        thread.join()


def main(max = 500):
    line="========================"
    print(f"Loading models:\n{line}")
    models = {}
    for id, model in {
        "BR": "Breton STT v0.1.1",
        "CA": "Catalan STT v0.14.0",
        "CY": "Welsh STT v21.03",
        "DE-AA": "German STT v0.9.0",
        "DE-JA": "German STT v0.0.1",
        "EN": "English STT v1.0.0-large-vocab",
        "ES": "Spanish STT v0.0.1",
        "EU": "Basque STT v0.1.1",
        "FR-CV": "French STT v0.6",
        "FR-JA": "French STT v0.0.1",
        "FY": "Frisian STT v0.1.1",
        "GA": "Irish STT v0.1.1",
        "IT-JA": "Italian STT v0.0.1",
        "IT-MI": "Italian STT 2020.8.7",
        "NL": "Dutch STT v0.0.1",
        "PT": "Portuguese STT v0.1.1"
    }.items():
        models[id] = STTModel(id, model)
    print(f"\nProcessing files\n{line}")
    print(f"Maximum of {max} per language")
    test_models(models, maxfiles=max)

# Run the tests
if __name__ == "__main__":
    main(max_g)

