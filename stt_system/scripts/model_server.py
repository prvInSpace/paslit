

import socket
import subprocess
import time

class ModelServer:
    """Used to encapsulate the model_server
    application and socket to allow for easier communication between
    them and the python program.
    """

    def __init__(self, ip='127.0.0.1', port=13990, modelfile="models.dat", timeout: int = 10):
        """Creates a new instance of the ident_lang_server 
        and stores a reference to the process in the object.
        A socket is also opened to the process after 3 seconds.

        Parameters:
            ip (str): The IP of the ident_lang_server
            port (int): The port of the server
            modelfile (str): The list of models the server should load
        """
        self.service = subprocess.Popen(["model_server", "-p", str(port), "-m", modelfile], stdout=subprocess.DEVNULL)
        start = time.time()
        end = (timeout * 1000 * 1000) + start
        connected = False
        while time.time() < end and not connected:
            time.sleep(0.1)
            try:
                self.socket = socket.socket() 
                self.socket.connect((ip, port))
                connected = True
            except Exception as e:
                if time.time() >= end:
                    raise e


    def __del__(self):
        """Kills the service and the socket properly
        Closing the socket might not be required,
        but killing the service probably is.
        """
        self.service.kill()
        self.socket.close()


    def get_bpc(self, text: str) -> float:
        """Sends the text to the model server and returns
        the returned bpc.

        Parameters:
            text (str): The text to classify
        """
        self.socket.send(text.encode())
        sbpc = self.socket.recv(1024).decode()
        return float(sbpc)
        
