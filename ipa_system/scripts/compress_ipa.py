#!/usr/bin/env python3

# Converts every token of an IPA file
# into a one byte number. This is useful
# because Tawa works on a character by
# character basis.
#
# Author: Preben Vangberg
# Date:   2021-10-19

import sys
from allosaurus.lm.inventory import Inventory
from allosaurus.model import get_model_path

# Fetch all of the different IPA symbols available
inventory = Inventory(get_model_path('latest'))
values = list(inventory.unit.id_to_unit.values())[1:]

if len(sys.argv) < 3:
    print("Usage: compress_ipa <infile> <outfile>")
    exit(1)

infile = sys.argv[1] 
outfile = sys.argv[2]

with open(outfile, 'wb') as output:
    with open(infile, 'r') as input:
        for line in input.readlines():
            for token in line.split():
                # Print the tokens index in the list of values
                # as a single byte
                try:
                    output.write((values.index(token) + 1).to_bytes(1, byteorder='big'))
                except Exception as e:
                    print(f"Failed to read {infile}. Unknown token '{token}'")
                    exit(1)
            output.write((0).to_bytes(1, byteorder='big'))
