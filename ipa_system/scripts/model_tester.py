

import socket
import subprocess
import time

class ModelTester:
    """Used to encapsulate the ident_lang_server
    application and socket while also allowing
    for storing of results and classification
    values.
    """

    results = {}
    possible_values = []

    def __init__(self, ip='127.0.0.1', port=13990, modelfile="models.dat"):
        """Creates a new instance of the ident_lang_server 
        and stores a reference to the process in the object.
        A socket is also opened to the process after 3 seconds.

        Parameters:
            ip (str): The IP of the ident_lang_server
            port (int): The port of the server
            modelfile (str): The list of models the server should load
        """
        self.service = subprocess.Popen(["ident_lang_server", "-m", modelfile], stdout=subprocess.DEVNULL)
        time.sleep(3)
        self.socket = socket.socket() 
        self.socket.connect((ip, port))


    def __del__(self):
        """Kills the service and the socket properly
        Closing the socket might not be required,
        but killing the service probably is.
        """
        self.service.kill()
        self.socket.close()


    def classify_lang(self, expected: str, text: str, debug=False) -> None:
        """Takes the text and classifies it.
        The data is then added to the results
        object so that a confusion matrix can
        be constructed.

        Parameters:
            expected (str): The expected classification
            text (str): The text to classify
            debug (bool): Whether to print debug information or not
        """
        if expected not in self.results:
            self.results[expected] = {}
        if expected not in self.possible_values:
            self.possible_values.append(expected)

        
        self.socket.send(text.encode())
        actual = self.socket.recv(1024).decode()
        if debug:
            print(f"Expected: {expected} Actual: {actual}")

        if actual not in self.results[expected]:
            self.results[expected][actual] = 0
        self.results[expected][actual] += 1 

        if actual not in self.possible_values:
            self.possible_values.append(actual)


    def print_confusion_matrix(self) -> None:
        """Prints the confusion matrix using the
        data stored in results object. The available
        values are stored in the possible_values variable.
        """
        print("", end="\t")
        for key in self.possible_values:
            print(key, end="\t")
        print()
        for expected in self.possible_values:
            if expected in self.results:
                print(expected, end="\t")
                for actual in self.possible_values:
                    if actual in self.results[expected]:
                        print(self.results[expected][actual], end="\t")
                    else:
                        print(0, end="\t")
                print()
        print()
