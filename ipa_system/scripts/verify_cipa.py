#!/usr/bin/env python3

#
# Author: Preben Vangberg
# Date:   2021-10-19

from io import BufferedReader
import re
import subprocess

def read_next_line(file: BufferedReader, sep=0):
    result = []
    while True:
        next = file.read(1)
        if next == sep:
            break
        result.append(next)
    return result


def main():
    models = get_model_names()
    results = {}
    results_length = {}
    languages = []
    for lang_code in models.keys():
        expected = models[lang_code]
        languages.append(expected)
        results[expected] = {}
        max = 0
        path = f"build/{lang_code}/{lang_code}_testing.cipa"
        print(f"Testing {path}")
        with open(path, "rb") as testdata:
            data = testdata.read()
            buffer = []
            for byte in data:
                if byte == 0:
                    max += 1
                    if max > 200:
                        break
                    lang = get_lang_for_sentence(buffer)
                    if lang not in results[expected]:
                        results[expected][lang] = 0
                    results[expected][lang] += 1

                    if len(buffer) not in results_length:
                        results_length[len(buffer)] = {}
                        results_length[len(buffer)]['total'] = 0
                        results_length[len(buffer)]['correct'] = 0

                    results_length[len(buffer)]['total'] += 1
                    if lang == expected:
                        results_length[len(buffer)]['correct'] += 1

                    buffer.clear()
                else:
                    buffer.append(byte)
    
    print_confusion_matrix(results, languages)
    print()
    print(results)
    print(results_length)

def print_confusion_matrix(matrix, possible_values):
    print("", end="\t")
    for key in possible_values:
        print(key, end="\t")
    print()
    for expected in possible_values:
        if expected in matrix:
            print(expected, end="\t")
            for actual in possible_values:
                if actual in matrix[expected]:
                    print(matrix[expected][actual], end="\t")
                else:
                    print(0, end="\t")
            print()
    
    print()

def get_lang_for_sentence(string: str) -> str:
    call = f'echo "{string}" | ident_lang_audio -m cipa_models.dat'
    for b_line in subprocess.Popen(call, shell=True, stdout=subprocess.PIPE).stdout.readlines():
        line = b_line.decode()
        print(line, end='')
        if line.find("Minimum codelength for") >= 0:
            return re.findall(r"(?<=Minimum codelength for ).*", line)[0]
    return None



# Loads the model names and language
# codes from the models.dat file
def get_model_names() -> dict:
    models = {}
    with open("cipa_models.dat") as input:
        for line in input.readlines():
            parts = line.split()
            langcode =  re.findall(r'(?<=/)[a-z]*(?=_cipa\.model)', parts[1])[0]
            models[langcode] = parts[0]
    return models

if __name__ == "__main__":
    main()