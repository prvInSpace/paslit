#!/usr/bin/env python3

# Splits the data in two parts.
# One being approximately 4/5 and
# one being approximately 1/5.
#
# Takes one parameter which is the
# language code. E.g "cy"
#
# Will create and overwrite the files:
# build/{lang}/{lang}.ipa
# build/{lang}/{lang}_testing.ipa
#
# Author: Preben Vangberg
# Date:   2021-10-19

import sys, os, random

if len(sys.argv) < 2:
    print("Usage: split_data <langcode>")
    exit(1)

lang = sys.argv[1] 
ext = sys.argv[2] 

input_path = f"build/{lang}/{ext}"
training_path = f"build/{lang}/{lang}.{ext}" 
testing_path  = f"build/{lang}/{lang}_testing.{ext}" 

print(f"Splitting files from {input_path} to {training_path}")

with open(training_path, 'wb') as training, open(testing_path, 'wb') as testing:
    for file in os.scandir(input_path):
        with open(file, 'rb') as input:
            if random.randrange(0,10) == 0:
                testing.write(input.read())
            else:
                training.write(input.read())

