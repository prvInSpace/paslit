#!/usr/bin/env python3

# Converts every token of an IPA file
# into a one byte number. This is useful
# because Tawa works on a character by
# character basis.
#
# Author: Preben Vangberg
# Date:   2021-10-19

import sys
from allosaurus.lm.inventory import Inventory
from allosaurus.model import get_model_path

# Fetch all of the different IPA symbols available
inventory = Inventory(get_model_path('latest'))
values = list(inventory.unit.id_to_unit.values())[1:]

if len(sys.argv) < 2:
    print("Usage: compress_ipa <infile> <outfile>")
    exit(1)

infile = sys.argv[1] 

with open(infile, 'rb') as input:
    while (byte := input.read(1)):
        index = int.from_bytes(byte, 'big')
        if index == 0:
            print()
        else: 
            print(values[index-1], end=" ")
        



