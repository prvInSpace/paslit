#!/usr/bin/env python3

#
# Author: Preben Vangberg
# Date:   2021-10-19

import re
import subprocess
import sys

sys.path.insert(1, 'scripts/')
from model_tester import ModelTester

def main():
    data = ModelTester()
    models = get_model_names()
    for lang_code in models.keys():
        expected = models[lang_code]
        for line in open(f"build/{lang_code}/{lang_code}_testing.ipa"):
            data.classify_lang(expected, line)

    data.print_confusion_matrix()



# Loads the model names and language
# codes from the models.dat file
def get_model_names() -> dict:
    models = {}
    with open("models.dat") as input:
        for line in input.readlines():
            parts = line.split()
            langcode =  re.findall(r'(?<=/)[a-z]*(?=\.model)', parts[1])[0]
            models[langcode] = parts[0]
    return models

if __name__ == "__main__":
    main()