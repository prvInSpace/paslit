
default: build

build:
	cd common; make -f Makefile
	cd stt_system; make -f Makefile

clean:
	make -f stt_system/Makefile clean
