# Automatic Spoken Language Classification

Work done for the ICE-4005 Generalist Mini-project and for a paper submitted to the Wales Academic Symposium on Language Technologies 2022.

Includes two different systems of classifiying spoken language. One using Allusaurus to extract IPA symbols and using compression-based language models to detect the language from that, and one using a combination of Speech-To-Text models and compression-based language models.

Most generated data is available for analysis if wanted. Audiofiles (for example from CommonVoice) needs to be added manually due to their size, other than that the repository should be fairly easy to set up.

## Maintainer
* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

## Citation

```bibtex
@inbook{vangberg2022sli,
    title={Using a combination of STT-models and compression-models to identify spoken language},
    booktitle={Welsh Academic Symposium on Language Technologies 2022},
    url={https://symposiwm.bangor.ac.uk/en},
    author={Vangberg, Preben},
    year={2022}
}
```

## Other notes

* Currently the acronyms for the program are PASLC (Prv's Automatic Spoken Language Classification) or PASLIT (Prv's Automatic Spoken Language Identification Tool). Not only are these a mouthful, but they are not even recursive or interesting. This needs to be resolved at some point.
